import {Page4} from './Page4';
import { withTime } from './withTime';
import { withLoading } from './withLoading';

export const Page3 = withLoading(withTime(Page4));
