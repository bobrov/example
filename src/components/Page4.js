import * as React from 'react';

export const Page4 = ({now}) => {
  return (
    <>
      <p>A lot of cool content!</p>
      <p>And by the way, the time is now { now.toLocaleTimeString() }</p>
    </>
  );
};
