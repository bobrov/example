import {Page4} from './Page4';
import { withTime } from './withTime';
import { withLoading } from './withLoading';
import {compose} from 'recompose';

export const Page5 = compose(
  withLoading,
  withTime
)(Page4);
