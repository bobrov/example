import * as React from 'react';

export const withLoading = (WrappedComponent) => ({isLoading, ...props}) => {
  if (isLoading) {
    return (
      <h4>The page is loading...</h4>
    );
  }
  return <WrappedComponent { ...props } />;
};
