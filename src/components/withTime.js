import * as React from 'react';

export const withTime = (WrappedComponent) => (props) => {
  return <WrappedComponent { ...props } now={ new Date() } />;
};
