import * as React from 'react';

export const Page = ({isLoading, now}) => {
  if (isLoading) {
    return (
      <h4>The page is loading...</h4>
    );
  }
  return (
    <>
      <p>A lot of cool content!</p>
      <p>And by the way, the time is now { now.toLocaleTimeString() }</p>
    </>
  );
};
