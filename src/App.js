import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Page } from './components/Page';
import { Page2 } from './components/Page2';
import { Page3 } from './components/Page3';
import { Page5 } from './components/Page5';

function App() {
  return (
    <div className="App">
      <Page isLoading={ false } now={ new Date() } />
      <Page2 isLoading={ false } />
      <Page3 isLoading />
      <Page5 isLoading />
    </div>
  );
}

export default App;
